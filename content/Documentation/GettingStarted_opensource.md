+++
title = "Using open-source Python code"
+++

*Warning: size of the total installation?*

- If you want to only _use_ BESOS, just open your favorite Python prompt and type: **pip install besos**
- If you want to be able to _make some changes_ in BESOS:
  -   Clone BESOS repository on your computer. 
  -   Open your favorite Python prompt and type: **pip install -r requirements.txt**