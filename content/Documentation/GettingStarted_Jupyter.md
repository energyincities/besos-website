+++
title = "Using Jupyter Notebooks"
+++

_You can use different programming languages but we advice you to write your scripts in Python 3, as all the tools proposed in BESOS are coded in Python 3._

#### Sign in

-   Go to page [XX](/) and click �Sign in� button or use your Gitlab account.
-   Fill the form. *(no commercial use?)*  
-   Wait for the email confirmation of your registration.
    

#### Create a new Jupyter Notebook
TODO

#### Play with Jupyter Notebooks
-   General documentation available [here](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/).
-   Look also at our nice [examples](/).
    

#### Make a Jupyter Notebook public
TODO

#### Share Jupyter Notebooks
TODO