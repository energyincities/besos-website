+++
weight = 1
title = "Multi-objective building optimization"
img = "img/portfolio/buildingoptimization.jpg"
link = "https://gke.mybinder.org/v2/gl/energyincities%2Fbesos-public/master?filepath=BuildingOptimization.ipynb"
type = "Optimization"
+++