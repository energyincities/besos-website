+++
weight = 1
title = "Energy hub storage visualization"
img = "img/portfolio/storage.jpg"
link = "https://gke.mybinder.org/v2/gl/energyincities%2Fbesos-public/master?filepath=StoragePlots.ipynb"
type = "Energy-Hub-Modelling"
+++