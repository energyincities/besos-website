+++
title = "Surrogate Models"
+++
###  Surrogate modelling
"A  **surrogate model** mimics the behavior of a simulation model as closely as possible while being computationally cheaper to evaluate. Surrogate models are constructed using a data-driven, bottom-up approach. The exact, inner working of the simulation code is not assumed to be known (or even understood), solely the input-output behavior is important. A model is constructed based on modeling the response of the simulator to a limited number of intelligently chosen data points. This approach is also known as behavioral modeling or black-box modeling, though the terminology is not always consistent." ([_Wikipedia_](https://en.wikipedia.org/wiki/Surrogate_model))

Besos proposes a machine learning toolbox for fitting surrogate models that approximate the behaviour of complex simulators.