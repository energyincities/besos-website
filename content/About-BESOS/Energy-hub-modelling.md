+++
title = "Energy Hub Modelling"
+++
## Energy Hub
An energy hub is a unit where multiple energy carriers can be converted and stored. It represents an interface between different energy infrastructures and/or loads.

<div  class="col-md-12 col-sm-12" style="padding-bottom:10px;">
<img  src="img/modals/EHub_example.png"  class="horizontal-center img-responsive"  alt="" />
<figcaption style="text-align: center;">Figure 1: Example of energy hub</figcaption>
</div>

The solution to energy hub problem determines the energy carriers input to be purchased and stored in order to satisfy energy request while minimizing a cost function.

The model can also be used to design the energy hub by allowing changes in sizing of the different elements during the optimization process.

### Model formulation
The energy hub relates a vector of energy inputs `$I$` to outputs `$L$` by means of a conversion matrix `$C$`, as shown in Eq. (1). The matrix `$C$` gives the efficiency of conversion between all inputs `$I$` to all outputs `$L$`, with zero terms if conversion is not possible. The problem is usually stated in terms of finding a combination of conversions that meet a specified demand `$L$`, which is derived from separate simulation or measurement of the system under consideration. The aim is often to minimize some objective function (Eq. (2)) that is stated in terms of coefficients `$F$` of the input energy streams `$I$`. This could represent energy prices (to minimize operating costs), carbon factors (to minimize emissions) or any other quantity.

The energy hub model becomes an optimization problem if decision variables `$P$` are assigned to all possible conversions, i.e. as coefficients to all non-zero terms in the conversion matrix `$C$`. It is advantageous to rearrange Eq. (1) to give Eq. (3) so that the new sparse form of the conversion matrix `$Q$` has one column per decision variable `$P$`. This gives one column per conversion rather than per output energy stream, giving greater flexibility (e.g. conversions to multiple streams, conversions from one output stream to another, and constraints that affect specific conversions). There may be constraints on the availability of certain energy inputs (Eq. (4)), for example from renewable generation, and there may be limits on each conversion due to the capacity of the plant Pmax (Eq. (5)).

For converters that are in series, the output energy stream of one device is used as the input energy stream for another, so negative coefficients are used in the conversion matrix. For example, a heat pump uses electricity (an output stream from PV or grid feed) to produce heat, so has a negative coefficient in the electricity stream and a positive coefficient in the heat stream. Converters that deliver more than one form of energy, for example combined heat and power (CHP) engines, would have positive coefficients in more than one row of the conversion matrix `$Q$`. Similarly converters that require two forms of energy input, for example heat pumps that uses electricity and low-grade heat, would have negative coefficients in more than one row.
`$$
\begin{aligned}
 &L = C \times I \qquad &(1) \\
&\min f = F \times I \qquad &(2)\\
&L = \Theta \times P \qquad &(3)\\
&I  \le I_{max} \qquad &(4)\\
&P_{min} \le P \le P_{max} \qquad &(5)
\end{aligned}
$$`
Due to the intermittent nature of renewable energy generation and demand, storage is often necessary to better match supply with demand. The following implementation of storage in the energy hub concept broadly follows that of [Parisio et al.](https://www.sciencedirect.com/science/article/pii/S0142061512000701) When energy stores are included in the model, for example hot water tanks or battery storage, two new variables are introduced per store: `$Q_{-}$`, the energy discharged from each store, and `$Q_{+}$`, energy used to charge each store. It is necessary to use two variables per store in order to apply different charging and discharging efficiencies (the matrices `$A_{-}$ and $A_{+}$` in Eq. (6)). If only one variable were used with positive values for charging and negative for discharging, it would not be possible to apply different efficiencies, and thus storage would be effectively lossless. In order to incorporate storage into the demand and supply balance (Eq. (3)), a transient version of the energy hub model is required as in Eq. (6) (in optimisation terms, [Dantzig](https://scholar.google.com/scholar_lookup?title=Linear%20programming%20and%20extensions&author=G.B.%20Dantzig&publication_year=1998) calls this a multi-stage model). Both the outputs `$L$` and the decision variables `$P$` are repeated for `$t = 1:N$` timesteps, as are the storage vectors `$Q_{-}$` and `$Q_{+}$`.
Storage continuity is enforced by Eq. (7), where the contents of each store E(t) at each timestep is equal to the contents at the previous timestep plus any charging and minus any discharging. Storage charging and discharging is coupled to the output energy streams by means of the charging and discharging efficiency matrices `$A_{-}$` and `$A_{+}$`. These play the same role as the conversion matrix $Q$ does for input energy streams. Because each store takes the same energy form in and out, each column has only one nonzero value, equal to the efficiency.
It is necessary to ensure that each store cannot charge or discharge simultaneously. This is achieved using the binary variables `$\delta^{i}_{-}$` and `$\delta^{i}_{+}$` (equal to 0 or 1), which are constrained by Eq. (8) such that both cannot be equal to 1. These are multiplied by the maximum charge or discharge per timestep `$Q^{max}_{-}$` and `$Q^{max}_{+}$` (the energy added or extracted in a given timestep), such that charging and discharging cannot occur together (one must have a maximum of zero in Eq. (9) due to the multiplication by `$\delta$`). Note that this requires mixed-integer linear programming to be used (see next section). Limits on the total capacity of each store are also imposed (Eq. (10)).

`$$
\begin{aligned}
 &L(t) = \Theta \times P(t) + A_{-}Q_{-} - A_{+}Q_{+} \qquad &(5) \\
&E(t+1) = E(t) + Q_{+}(t) - Q_{-}(t) \qquad &(6)\\
&\delta^{i}_{-}(t) + \delta^{i}_{+}(t) \le 1 \qquad &(7)\\
&0\le Q_{-}(t)  \le Q^{max}_{-}\delta^{i}_{-}(t) \qquad &(8)\\
&0\le Q_{+}(t)  \le Q^{max}_{+}\delta^{i}_{+}(t) \qquad &(9) \\
&E_{min} \le E(t) \le E_{max} &(10)
\end{aligned}
$$`
  
### Model solving
CPLEX solver uses a branch and cut algorithm to solve MILP.