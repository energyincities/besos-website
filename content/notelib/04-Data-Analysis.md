+++
weight = 4
title = "Data Analysis"
description = "<li> Smart thermostat data analysis (issue with sharing data?).</li> <br> <li> Building electricity estimation for PV array sizing.</li> <br>"
img = "img/portfolio/MOSES-left.jpg"
link = "https://besos.uvic.ca"
+++