+++
weight = 1
title = "Surrogate Modelling"
description = "<li> Surrogate modelling of building energy simulation.</li> <br> <li> Adaptive sampling for surrogate model fitting.</li> <br>"
img = "img/portfolio/MOSES-left.jpg"
link = "https://besos.uvic.ca"
+++