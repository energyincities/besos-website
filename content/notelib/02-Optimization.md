+++
weight = 2
title = "Optimization"
description = "<li> Multi-objective building optimization.</li> <br> <li> Surrogate-based building optimization.</li><br>"
img = "img/portfolio/MOSES-left.jpg"
link = "https://besos.uvic.ca"
+++