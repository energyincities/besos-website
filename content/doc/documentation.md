#### Getting started

To get started with BESOS the Platform we have provided [this 2-page pamphlet](https://gitlab.com/energyincities/besos/-/raw/master/docs/Getting%20Started%20on%20BESOS%20and%20making%20%20your%20first%20Notebook.pdf?inline=false) that will get you from signup to your first notebook. 

For all information on making use of besos library and locally installing it on your computer review the [Read The Docs](https://besos.readthedocs.io)

Finding more info on EnergyHub modelling review the [Read the Docs PyEhub](https://python-ehub.readthedocs.io) to get details of specific classes.
