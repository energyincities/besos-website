**The BESOS Platform is a cloud-based portal of modular, reusable software components for researchers to perform integrated building and energy systems analysis.**
<br>Buildings, renewable energy generation and storage technologies and associated energy systems all pose complex, interacting design and operational challenges. 
Finding high-performing solutions to these problems requires a new generation of computational tools, blending aspects of simulation, optimization, machine learning and visualization.
<br>The project is funded by a grant from [CANARIE](https://www.canarie.ca/).

**Integration of the following core modules:**

* Building energy simulation via parametric [EnergyPlus](https://energyplus.net/) model execution building on [EPPy](https://pypi.org/project/eppy/).
* Multi-objective optimization using [Platypus](https://github.com/Project-Platypus/Platypus) to provide 10+ evolutionary algorithms.
* Machine learning tools for fitting <a data-toggle="modal" href="#surrogate-models-Modal">surrogate models</a> that approximate the behavior of complex simulators, implemented with [scikit-learn](https://scikit-learn.org) and [TensorFlow](https://www.tensorflow.org).
* <a data-toggle="modal" href="#Energy-hub-modelling-Modal">Energy hub modelling</a> that balances demand and supply and sizes converters and storages in multi-energy systems using [PyEHub](https://gitlab.com/energyincities/python-ehub).

**Modules are accessible by:**

* Using interactive tools in the online portal.
* Using [Jupyter Notebooks](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html) within the portal, giving ease of use for non-programmers.
* pip installing the open-source Python repositories to run locally, accessed via the API.

**The main technologies that underpin the platform are:**

* [Jupyter Hub](https://jupyter.org/hub) to run a notebook server for each user who logs in.
* [Docker](https://www.docker.com/) to make sure these notebooks run independently. 
* [mybinder.org](https://mybinder.org) to make notebooks publically accessible, which are embedded as public tutorials.