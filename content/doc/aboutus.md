The BESOS platform is developed by the [Energy in Cities group](https://energyincities.gitlab.io/website/) at the [University of Victoria](https://www.uvic.ca/) in beautiful British Columbia, Canada.

The group is led by [Dr. Ralph Evins](http://energyincities.gitlab.io/website/#team).

#### Contact us
+ You can tell us about specific things on GitLab by raising issues in the repositories ([BESOS](https://gitlab.com/energyincities/besos/issues), [PyEHub](https://gitlab.com/energyincities/python-ehub/issues)).
+ Add to the 'how-to' documentation by editing the wiki pages for each repository ([BESOS](https://gitlab.com/energyincities/besos/wikis/home), [PyEHub](https://gitlab.com/energyincities/python-ehub/wikis/home)).
+ Contact the development team by emailing <a href="mailto:besos@uvic.ca">besos@uvic.ca</a>.

#### Key users
We are working with the following research teams to develop specific components and case studies:

+ [Dr Curran Crawford](https://www.uvic.ca/engineering/mechanical/faculty-and-staff/faculty/curranc.php) ([Sustainable Systems Design lab](https://www.ssdl.uvic.ca/), [UVic](https://www.uvic.ca/)) - probabilistic modelling, wind power, electric vehicle applications.
+ [Dr Andrew Rowe](https://www.uvic.ca/engineering/mechanical/faculty-and-staff/faculty/arowe.php) ([IESVic](https://www.uvic.ca/research/centres/iesvic/index.php)) - grid integration issues in BC.
+ [Dr Adam Rysanek](https://sala.ubc.ca/people/faculty/adam-rysanek) ([&eta; lab](https://blogs.ubc.ca/etalab/), [UBC](https://www.ubc.ca/)) - architecture, building data.
+ [Dr Jean Duquette](https://carleton.ca/mae/people/jean-duquette/) ([Carleton](https://carleton.ca/)) - district systems, renewable energy integration, waste heat recovery and energy storage.
+ [Dr Madeleine McPherson](https://www.uvic.ca/engineering/civil/people/home/mmcpherson.php) - renewable energy systems integration.
+ [Dr David Bristow](https://www.uvic.ca/engineering/civil/people/home/dbristow.php) - resilience assessment.
