+++
weight = 3
img = "img/portfolio/secure.png"
+++

### NOTEBOOKS AS APPS
Explore your model by interactive visualization using sliders/buttons/dropdowns and pretty graphics. All the code is hidden but accessible to power users.