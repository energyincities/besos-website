+++
weight = 11
img = "img/portfolio/secure.png"
+++
### SHARE
An executable paper! Make your notebook public with a static link and frozen dependencies. Include the link in your publications to encourage others to build on (and cite!) your work. **Coming soon**