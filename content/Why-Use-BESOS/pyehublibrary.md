+++
weight = 9
img = "img/portfolio/secure1.png"
+++
<!-- here an image of size 1428x1216 must be used for proper layout. -->
### PyEHub LIBRARY
Our [PyEHub library](https://gitlab.com/energyincities/python-ehub/) for <a data-toggle="modal" href="#Energy-hub-modelling-Modal">energy hub modelling of energy systems</a> provides an easy way to define energy balancng and system sizing problems.
