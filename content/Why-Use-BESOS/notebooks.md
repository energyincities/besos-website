+++
weight = 1
img = "img/portfolio/secure.png"
+++

### NOTEBOOKS

[Python notebooks](https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html)  are great for mixing executable code, visual outputs and formatted explanations.
<br><br><br><br>