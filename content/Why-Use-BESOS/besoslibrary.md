+++
weight = 8
img = "img/portfolio/secure1.png"
+++
<!-- here an image of size 1428x1216 must be used for proper layout. -->
### BESOS LIBRARY
Our [BESOS library](https://gitlab.com/energyincities/besos/) for <a data-toggle="modal" href="#surrogate-models-Modal">optimization and surrogate modelling of buildings</a> provides wrappers for [EnergyPlus](https://energyplus.net/) via [EPPy](https://pypi.org/project/eppy/) for energy simulation, [Platypus](https://github.com/Project-Platypus/Platypus) for optimization and [scikit-learn](https://scikit-learn.org/) and [TensorFlow](https://www.tensorflow.org/) for surrogate modelling.
