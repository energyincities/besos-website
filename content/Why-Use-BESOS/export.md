+++
weight = 8
img = "img/portfolio/secure.png"
+++
### EXPORT
Easily move your work locally if desired, just download your notebooks and follow the instructions to replicate the environment.
<br><br><br>