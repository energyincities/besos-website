+++
weight = 2
img = "img/portfolio/secure.png"
+++

### CLOUD
Run notebooks on our cloud resources, with a pre-configured environment where all libraries and other software ([EnergyPlus](https://energyplus.net/), [CPlex](https://www.ibm.com/analytics/cplex-optimizer)) are automatically kept up to date.
<br><br>