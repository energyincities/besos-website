### Summary

The buildings sector is one of the largest contributors to carbon emissions, up to 33% of global CO$_2$ emissions. Improved computational methods are needed to help design more energy-efficient buildings. besos is a Python library and associated web-based platform to help researchers and practitioners to explore energy use in buildings more effectively. This is achieved by providing an easy way of integrating many disparate aspects of building modelling, district modelling, optimization and machine learning into one common library.

### besos library and associated BESOS Platform

The BESOS Platform is a Jupyter Hub that is able to run the besos code base, and has all the dependencies installed. The platform is freely accesible for academics in the building energy modelling space. To get started with BESOS the Platform we have provided [this 2-page pamphlet](https://gitlab.com/energyincities/besos/-/raw/master/docs/Getting%20Started%20on%20BESOS%20and%20making%20%20your%20first%20Notebook.pdf?inline=false) that will get you from signup to your first notebook. 

The besos library connects modelling and optimisation tools to allow for parameterizing running and optimizing models, sampling from their design spaces, and generating data for use in machine learning models. This supports designing building and district energy modelling experiments.